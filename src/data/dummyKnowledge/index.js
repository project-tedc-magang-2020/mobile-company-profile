import {Knowledge1, Knowledge2} from '../../assets'

export const dummyKnowledge = [
    {
        id: 1,
        image: Knowledge1,
        judul: 'Industri Metaverse Berkembang Pesat, Bagaimana Dukungan Pemerintah ?',
        tanggal : '25 Mei 2022',
        konten: 'Perkembangan industri metaverse dinilai potensial dan semakin cepat ke depannya. Pemerintah pun bersiap menyediakan infrastrukturnya.',
        isi: 'Perkembangan industri metaverse dinilai potensial dan semakin cepat ke depannya. Pemerintah pun bersiap menyediakan infrastrukturnya. Menteri Komunikasi dan Informatika Johnny G Plate mengakui industri metaverse berkembang pesat. Dia mengatakan Indonesia tak boleh ketinggalan menangkap peluang ekonomi digital ini. "Metaverse berkembang pesat. Pada saat kita menyiapkan 4G dan 5G di saat itu akan tumbuh dan berkembang sangat besar. Kita harus mengambil inisiatif. Pemerintah punya komitmen," ujar Johnny G Plate di sela acara World Economic Forum 2022 di Davos, Selasa (25/5/2022).'
    },
    {
        id: 2,
        image: Knowledge2,
        judul: 'Pencipta iPod Kritik Metaverse: Merusak Hubungan Manusia',
        tanggal : '19 Mei 2022',
        konten : 'Tak semua terkesan dengan konsep metaverse, dunia virtual di mana orang bisa saling berinteraksi dengan avatar masing-masing.',
        isi: 'Tak semua terkesan dengan konsep metaverse, dunia virtual di mana orang bisa saling berinteraksi dengan avatar masing-masing. Salah satunya adalah Tony Fadell yang dikenal sebagai bapak iPod. Fadell dulu adalah Senior Vice President di Apple yang menangani teknologi iPod. Ia berkomentar bahwa metaverse berisiko merusak interaksi antar manusia. Itu karena orang tak lagi berhadapan tatap muka secara fisik."Jika kalian memberikan teknologi di antara koneksi manusia itu, maka di situlah terjadi hal toxic," kata dia, seperti dikutip detikINET dari BBC.'
    },
]