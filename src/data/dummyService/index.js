import { ITManagedService, ITManagementConsultant, SoftwareDevelopment } from '../../assets'

export const dummyService = [
    {
        id: 1,
        image: ITManagementConsultant,
        service: 'IT MANAGEMENT CONSULTANT',
        desc: 'Memberi konsultasi atau nasihat kepada perusahaan-perusahaan mengenai cara terbaik untuk mengelola dan mengoperasikan bisnis di bidang IT.',
    },
    {
        id: 2,
        image: SoftwareDevelopment,
        service: 'SOFTWARE DEVELOPMENT',
        desc: 'Proyek IT yang berfokus pada penciptaan atau pengembangan perangkat lunak.',
    },
    {
        id: 3,
        image: ITManagedService,
        service: 'IT MANAGED SERVICE',
        desc: 'Penyedia layanan atau jasa untuk mengelola sistem atau infrastruktur yang di dalamnya mencakup mengelola sistem keamanan pada server, melakukan proses backup dan pengelolaan database, memantau kesehatan server, dan lainnya.',
    },
]; 