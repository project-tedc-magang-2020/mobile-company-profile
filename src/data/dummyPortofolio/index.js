import { Portofolio1, Portofolio2, Portofolio3, Portofolio4, Portofolio5, Portofolio6, Portofolio7, Portofolio8, Portofolio9, Portofolio10, Portofolio11 } from '../../assets'

export const dummyPortofolio = [
    {
        id: 1,
        image: Portofolio1,
        project: 'NAGARI COMPLIANCE MANAGEMENT SYSTEM (NCMS)',
        client: 'PT. Bank Pembangunan Daerah Sumatra Barat',
    },
    {
        id: 2,
        image: Portofolio2,
        project: 'COMPLIANCE REGULATORY & MONITORING SYSTEM (CRMS)',
        client: 'PT. Bank Tabungan Negara (Persero) Tbk',
    },
    {
        id: 3,
        image: Portofolio3,
        project: 'COMPLIANCE MANAGEMENT SYSTEM (CMS)',
        client: 'PT. Reasuransi Indonesia Utama (Persero)',
    },
    {
        id: 4,
        image: Portofolio4,
        project: 'APLIKASI TINGKAT KESEHATAN BANK (TKB) Bank BPD JATENG',
        client: 'PT. Bank Pembangunan Daerah Jawa Tengah',
    },
    {
        id: 5,
        image: Portofolio5,
        project: 'KUISIONER PENERAPAN PRINSIP TATA KELOLA SISTEM',
        client: 'Dapen Telkom',
    },
    {
        id: 6,
        image: Portofolio6,
        project: 'SISTEM INFORMASI RENCANA KERJA PEMBANGUNAN DAERAH (RKPD ONLINE)',
        client: 'BAPPEDA  Kab. Bekasi',
    },
    {
        id: 7,
        image: Portofolio7,
        project: 'SISTEM INFORMASI BDV',
        client: 'Bandung Digital Valley',
    },
    {
        id: 8,
        image: Portofolio8,
        project: 'SISTEM INFORMASI GEOGRAFIS',
        client: 'Dinas Kecamatan Bandung Wetan',
    },
    {
        id: 9,
        image: Portofolio9,
        project: 'SISTEM INFORMASI',
        client: 'Politeknik POS Indonesia',
    },
    {
        id: 10,
        image: Portofolio10,
        project: 'BALATRANS JABAR',
        client: 'Dinas Tenaga Kerja dan Transmigrasi Provinsi Jawa Barat',
    },
    {
        id: 11,
        image: Portofolio11,
        project: 'SISTEM INFORMASI',
        client: 'SMKN 4 Padalarang',
    },

]