import { BackendDeveloper, FrontEndDeveloper, MobileDeveloper, UIUXDesigner } from '../../assets'

export const dummyCareer = [
  {
    id: 1,
    image: BackendDeveloper,
    posisi: 'Backend Developer',
    pendidkan: 'Sarjana / S1',
    gaji: 'Rp. 5.000.000 - Rp. 10.000.000',
    pengalaman: '3 - 5 Tahun',
    kualifikasi: [
        'Menguasai Bahasa pemrograman JavaScript',
        'Menguasai Framework NodeJS',
        'Menguasai Database management',
        'Menguasai Server management',
        'Menguasai Framework backend',
        'Menguasai API'
    ],
    deskripsi:
      'Backend Developer atau Back End Developer adalah tim dari web developer yang memiliki tugas khusus untuk pengelolaan server, aplikasi serta database sehingga semua bisa berjalan dengan lancar.',
  },
  {
    id: 2,
    image: FrontEndDeveloper,
    posisi: 'Front-End Developer',
    pendidkan: 'Sarjana / S1',
    gaji: 'Rp. 5.000.000 - Rp. 10.000.000',
    pengalaman: '3 - 5 Tahun',
    kualifikasi: [
        'Menguasai Bahasa pemrograman JavaScript',
        'Menguasai Framework NodeJS',
        'Menguasai Database management',
        'Menguasai Server management',
        'Menguasai Framework backend',
        'Menguasai API'
    ],
    deskripsi:
      'Backend Developer atau Back End Developer adalah tim dari web developer yang memiliki tugas khusus untuk pengelolaan server, aplikasi serta database sehingga semua bisa berjalan dengan lancar.',
  },
  {
    id: 3,
    image: MobileDeveloper,
    posisi: 'Mobile Developer',
    pendidkan: 'Sarjana / S1',
    gaji: 'Rp. 5.000.000 - Rp. 10.000.000',
    pengalaman: '3 - 5 Tahun',
    kualifikasi: [
        'Menguasai Bahasa pemrograman JavaScript',
        'Menguasai Framework NodeJS',
        'Menguasai Database management',
        'Menguasai Server management',
        'Menguasai Framework backend',
        'Menguasai API'
    ],
    deskripsi:
      'Backend Developer atau Back End Developer adalah tim dari web developer yang memiliki tugas khusus untuk pengelolaan server, aplikasi serta database sehingga semua bisa berjalan dengan lancar.',
  },
  {
    id: 4,
    image: UIUXDesigner,
    posisi: 'UI/UX Designer',
    pendidkan: 'Sarjana / S1',
    gaji: 'Rp. 5.000.000 - Rp. 10.000.000',
    pengalaman: '3 - 5 Tahun',
    kualifikasi: [
        'Menguasai Bahasa pemrograman JavaScript',
        'Menguasai Framework NodeJS',
        'Menguasai Database management',
        'Menguasai Server management',
        'Menguasai Framework backend',
        'Menguasai API'
    ],
    deskripsi:
      'Backend Developer atau Back End Developer adalah tim dari web developer yang memiliki tugas khusus untuk pengelolaan server, aplikasi serta database sehingga semua bisa berjalan dengan lancar.',
  },
];
