import BackendDeveloper from './BackendDeveloper.png'
import FrontEndDeveloper from './FrontEndDeveloper.png'
import MobileDeveloper from './MobileDeveloper.png'
import UIUXDesigner from './UIUXDesigner.jpg'

export {
    BackendDeveloper,
    FrontEndDeveloper,
    MobileDeveloper,
    UIUXDesigner,
}