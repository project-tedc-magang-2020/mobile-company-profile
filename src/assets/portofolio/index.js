import Portofolio1 from './porto1.jpg';
import Portofolio2 from './porto2.jpg';
import Portofolio3 from './porto3.jpg';
import Portofolio4 from './porto4.jpg';
import Portofolio5 from './porto5.jpg';
import Portofolio6 from './porto6.jpg';
import Portofolio7 from './porto7.jpg';
import Portofolio8 from './porto8.jpg';
import Portofolio9 from './porto9.jpg';
import Portofolio10 from './porto10.jpg';
import Portofolio11 from './porto11.jpg';

export {
  Portofolio1,
  Portofolio2,
  Portofolio3,
  Portofolio4,
  Portofolio5,
  Portofolio6,
  Portofolio7,
  Portofolio8,
  Portofolio9,
  Portofolio10,
  Portofolio11,
};
