import Logo from './logo.svg'
import HomeImage from './home-image.svg'
import SegmentasiPerusahaan1 from './SegmentasiPerusahaan1.png'
import SegmentasiPerusahaan2 from './SegmentasiPerusahaan2.png'
import SegmentasiPerusahaan3 from './SegmentasiPerusahaan3.png'

export { Logo, HomeImage, SegmentasiPerusahaan1, SegmentasiPerusahaan2, SegmentasiPerusahaan3 }