import ITManagedService from './IT-MANAGED-SERVICE.png';
import ITManagementConsultant from './IT-MANAGEMENT-CONSULTANT.png';
import SoftwareDevelopment from './SOFTWARE-DEVELOPMENT.png';

export { ITManagedService, ITManagementConsultant, SoftwareDevelopment };