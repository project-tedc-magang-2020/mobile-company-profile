import Home from './home.svg';
import HomeActive from './home-active.svg';
import About from './about.svg';
import AboutActive from './about-active.svg';
import ServiceBTN from './service-btn.svg';
import KnowledgeBTN from './knowledge-btn.svg';
import CareerBTN from './career-btn.svg';
import PortofolioBTN from './portofolio-btn.svg';
import Contact from './contact.svg';
import ContactActive from './contact-active.svg';
import Address from './address.svg';
import Email from './email.svg';
import SuccessProject from './success-project.svg';
import Client from './client.svg';

export {
  Home,
  HomeActive,
  About,
  AboutActive,
  Contact,
  ContactActive,
  ServiceBTN,
  KnowledgeBTN,
  CareerBTN,
  PortofolioBTN,
  Address,
  Email,
  SuccessProject,
  Client
};
