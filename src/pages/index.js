import Home from './Home'
import Splash from './Splash'
import About from './About'
import Contact from './Contact'
import Career from './Career'
import Knowledge from './Knowledge'
import Portofolio from './Portofolio'
import Service from './Service'

export{ Home, Splash, About, Contact, Career, Knowledge, Portofolio, Service }