import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import {ListService} from '../../components';
import LinearGradient from 'react-native-linear-gradient';
import {colors, fonts} from '../../utils';
import { dummyService } from '../../data';

export default class Service extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       services: dummyService
    }
  }
  render() {
    const { services } = this.state
    return (
      <ScrollView style={{backgroundColor: 'white', flex: 1}}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={[colors.primary, colors.secondary]}
          style={styles.container}>
          <View style={styles.headerTextSection}>
            <Text style={styles.headerText}>
              PT. Crop Inspirasi Digital adalah perusahaan yang bergerak di
              bidang Teknologi Informasi dan Komunikasi (TIK). Untuk saat ini
              kami telah membuka layanan diantaranya:
            </Text>
          </View>
        </LinearGradient>
        <View style={styles.listService}>
          <ListService services={services}/>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 200,
  },
  headerTextSection: {
    marginTop: 30,
    marginHorizontal: 15,
  },
  headerText: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: '#FFFFFF',
    textAlign: 'center',
    lineHeight: 25,
  },
  listService: {
    marginHorizontal: 20,
  }
});
