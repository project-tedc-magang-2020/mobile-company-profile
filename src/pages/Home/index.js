import {StyleSheet, View, Text, TouchableOpacity, ScrollView} from 'react-native';
import React, {Component} from 'react';
import {CardHeaderComponent, HeaderComponent, SegmentasiPerusahaan} from '../../components';
import {CareerBTN, KnowledgeBTN, PortofolioBTN, ServiceBTN} from '../../assets';
import { colors, fonts } from '../../utils';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    return (
      <ScrollView style={styles.page}>
        <HeaderComponent />
        <CardHeaderComponent />
        <View style={styles.container}>
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Service')}>
              <ServiceBTN />
            </TouchableOpacity>
            <Text style={styles.text}>Service</Text>
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Knowledge')}>
              <KnowledgeBTN />
            </TouchableOpacity>
            <Text style={styles.text}>Knowledge</Text>
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Career')}>
              <CareerBTN />
            </TouchableOpacity>
            <Text style={styles.text}>Career</Text>
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Portofolio')}>
              <PortofolioBTN />
            </TouchableOpacity>
            <Text style={styles.text}>Portofolio</Text>
          </View>
        </View>
        <View style={styles.headerTextSection}>
          <Text style={styles.headerText}>Segmentasi Perusahaan</Text>
          <Text style={styles.text}>PT. Crop Inspirasi Digital memfokuskan layanan ke beberapa bidang yaitu:</Text>
        </View>
        <SegmentasiPerusahaan />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  container: {
    marginHorizontal: 30,
    marginVertical: 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  button: {
    alignItems: 'center',
  },
  text: {
    fontSize: 12,
    fontFamily: fonts.primary.regular,
    marginTop: 5,
    color:'grey'
  },
  image: {
    position: 'absolute',
    bottom: 0,
    width: 300,
    height: 300,
    alignSelf: 'center',
  },
  headerTextSection: {
    marginHorizontal: 30,
  },
  headerText: {
    fontSize: 20,
    color: colors.darkgrey,
    fontFamily: fonts.primary.bold,
  },
});
