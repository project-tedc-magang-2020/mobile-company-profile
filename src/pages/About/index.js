import {Text, StyleSheet, View, ScrollView, Image} from 'react-native';
import React, {Component} from 'react';
import {AboutCardComponent} from '../../components';
import { colors, fonts } from '../../utils';

export default class About extends Component {
  render() {
    return (
      <ScrollView style={styles.page}>
        <View style={styles.headerTextSection}>
          <Text style={styles.headerText}>About Us</Text>
        </View>
        <View style={styles.logoWrapper}>
          <Image source={require('../../assets/images/logo.png')} />
        </View>
        <Text style={styles.textAbout}>
          Menciptakan layanan pengembangan IT yang kreatif untuk kepentingan
          pelanggan, internal perusahaan, UMKM dan usaha lainnya. Yang
          mengembangkan layanan IT yang berlandaskan pada pemanfaatan teknologi
          informasi yang bernilai tinggi untuk menjadi perusahaan pengembangan
          IT terbsesar dan terbaik di Indonesia
        </Text>
        <AboutCardComponent />
        <View style={styles.headerTextSection}>
          <Text style={styles.headerText}>Legalitas Perusahaan</Text>
          <Text style={{ color: 'black', marginTop: 20, fontFamily: fonts.primary.regular, fontSize: 13 }}>• KEMENTRIAN HUKUM DAN HAK ASASI MANUSIA RI</Text>
        </View>
        <Image source={require('../../assets/images/legalitas.png')} style={styles.image}/>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerTextSection: {
    marginTop: 15,
    marginHorizontal: 30,
  },
  headerText: {
    fontSize: 20,
    fontFamily: fonts.primary.bold,
    color: colors.balck,
  },
  logoWrapper: {
    marginTop: 30,
    alignItems: 'center',
  },
  textAbout: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: 'black',
    marginHorizontal: 30,
    marginVertical: 20,
  },
  image:{
    marginHorizontal: 30,
    marginBottom: 20,
    marginTop: 10,
  }
});
