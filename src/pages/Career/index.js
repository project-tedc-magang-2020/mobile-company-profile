import {Text, StyleSheet, View, ScrollView, Image} from 'react-native';
import React, {Component} from 'react';
import {colors, fonts} from '../../utils';
import LinearGradient from 'react-native-linear-gradient';
import { ListCareer } from '../../components';
import { dummyCareer } from '../../data';

export default class Career extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       careers: dummyCareer
    }
  }
  render() {
    const { careers } = this.state;
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={{backgroundColor: 'white', flex: 1}}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={[colors.primary, colors.secondary]}
          style={styles.container}>
          <Text style={styles.headerTextBold}>
            Mulai Kariermu Bersama Kami!!!
          </Text>
          <Text style={styles.headerText}>
            Kami merangkul dan mengembangkan para talenta muda. Namun, kami juga
            membutuhkan individu yang memiliki jiwa kepemimpinan dan tentunya
            berpengalaman. Mulai perjalanan kariermu bersama kami!
          </Text>
        </LinearGradient>
        <View style={styles.pilihCareer}>
          <ListCareer careers={careers} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 15,
    borderRadius: 10,
  },
  headerText: {
    fontSize: 14,
    fontFamily: fonts.primary.regular, 
    color: '#FFFFFF',
    textAlign: 'center',
    lineHeight: 25,
    marginBottom: 20,
    marginHorizontal: 10,
  },
  headerTextBold: {
    fontSize: 14 ,
    color: '#FFFFFF',
    textAlign: 'center',
    fontFamily: fonts.primary.bold, 
    marginTop: 20,
  },
  pilihCareer: {
    marginHorizontal: 15,
    marginTop: 10,
  }
});
