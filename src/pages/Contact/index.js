import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import { ContactComponent,SendMessageComponent } from '../../components';
import { fonts } from '../../utils';

export default class Contact extends Component {
  render() {
    return (
      <ScrollView style={styles.page}>
        <View style={styles.headerTextSection}>
          <Text style={styles.headerText}>Contact Info</Text>
        </View>
        <ContactComponent/>
        <View style={styles.headerTextSection}>
          <Text style={styles.headerText}>Send Message</Text>
        </View>
        <SendMessageComponent/>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  headerTextSection: {
    marginTop: 15,
    marginHorizontal: 30,
  },
  headerText: {
    fontSize: 20,
    fontFamily: fonts.primary.bold,
    color: '#585454',
  },
});
