import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import {ListPortofolio} from '../../components';
import {dummyPortofolio} from '../../data';
import LinearGradient from 'react-native-linear-gradient';
import {colors, fonts} from '../../utils';

export default class Portofolio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      portofolios: dummyPortofolio,
    };
  }

  render() {
    const {portofolios} = this.state;
    return (
      <ScrollView style={{backgroundColor: 'white', flex: 1}}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={[colors.primary, colors.secondary]}
          style={styles.container}>
          <View style={styles.portfolio}>
            <Text style={styles.label}>
              Hasil karya-karya terhebat kami bersama para patner, yang telah
              teruji dan terbukti memberikan nilai tambah lebih untuk kesuksesan
              bersama, baik swasta maupun pemerintah yang telah mempercayakan
              amanahnya kepada kami.
            </Text>
          </View>
        </LinearGradient>
        <ListPortofolio portofolios={portofolios} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    height: 250,
  },
  portfolio: {
    marginTop: 10,
    marginHorizontal: 20,
  },
  label: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: '#FFF',
    textAlign: 'center',
    lineHeight: 25,
  },
});
