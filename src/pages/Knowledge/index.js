import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import {ListKnowledge} from '../../components';
import {dummyKnowledge} from '../../data';

export default class Knowledge extends Component {
  constructor(props) {
    super(props);

    this.state = {
      knowledges: dummyKnowledge,
    };
  }
  render() {
    const {knowledges} = this.state;
    return (
      <ScrollView style={styles.container}>
        <ListKnowledge knowledges={knowledges} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
});
