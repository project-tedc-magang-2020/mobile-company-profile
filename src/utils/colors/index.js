export const colors = {
    primary: '#F26322',
    secondary: '#F89621',
    orangetint: '#FFEDE3',
    grey: '#F1F1F1',
    darkgrey: '#4F4F4F',
    black: '#000000',
}