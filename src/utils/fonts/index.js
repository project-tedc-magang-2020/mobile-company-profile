export const fonts = {
    primary: {
        light: 'Poppins-Light',
        medium: 'Poppins-Medium',
        regular: 'Poppins-Regular',
        semiBold: 'Poppins-SemiBold',
        bold: 'Poppins-Bold',
    }
}