import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Home, Splash, About, Career, Contact, Knowledge, Portofolio, Service } from '../pages';
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomNavigator } from '../components';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />} >
      <Tab.Screen name="Home" component={Home} options={{ headerShown: false }} />
      <Tab.Screen name="About" component={About} options={{ headerShown: false }} />
      <Tab.Screen name="Contact" component={Contact} options={{ headerShown: false }} />
    </Tab.Navigator>
  );
}

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Splash">
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Service"
        component={Service}
      />
      <Stack.Screen
        name="Knowledge"
        component={Knowledge}
      />
      <Stack.Screen
        name="Career"
        component={Career}
      />
      <Stack.Screen
        name="Portofolio"
        component={Portofolio}
      />
    </Stack.Navigator>
  );
};

export default Router;
