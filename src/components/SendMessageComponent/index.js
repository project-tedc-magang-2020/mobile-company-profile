import {
  TextInput,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import React, {Component} from 'react';
import { colors, fonts } from '../../utils';

export default class SendMessageComponent extends Component {
  render() {
    return (
      <View style={{marginBottom: 30}}>
        <TextInput style={styles.input} placeholder="Name" placeholderTextColor={'grey'}></TextInput>
        <TextInput style={styles.input} placeholder="Email" placeholderTextColor={'grey'}></TextInput>
        <TextInput style={styles.input} placeholder="Subject" placeholderTextColor={'grey'}></TextInput>
        <TextInput
          style={styles.inputArea}
          placeholder="Message"
          placeholderTextColor={'grey'}
          multiline={true}
          numberOfLines={10}></TextInput>
        <TouchableOpacity style={styles.btn}>
          <Text style={styles.btnText}> Send </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: 50,
    borderWidth: 1,
    marginHorizontal: 30,
    paddingLeft: 15,
    marginTop: 20,
    borderRadius: 10,
    borderColor: 'grey',
    fontFamily: fonts.primary.regular,
  },
  inputArea: {
    height: 50,
    borderWidth: 1,
    marginHorizontal: 30,
    paddingLeft: 15,
    marginTop: 20,
    borderRadius: 10,
    borderColor: 'grey',
    textAlignVertical: 'top',
    height: 100,
  },
  btn: {
    height: 50,
    marginHorizontal: 30,
    marginTop: 30,
    borderRadius: 10,
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 20,
    fontFamily: fonts.primary.bold,
    color: '#FFFFFF',
  },
});
