import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { colors, fonts } from '../../utils'

const CardCareer = ({career}) => {
  return (
    <View>
        <TouchableOpacity style={styles.card}>
            <Image source={career.image} style={styles.gambar} />
        <Text style={styles.position}>{career.posisi}</Text>
        </TouchableOpacity>

        <TouchableOpacity>
            <Text style={styles.tombol}>Apply Job</Text>
        </TouchableOpacity>
    </View>
  )
}

export default CardCareer

const styles = StyleSheet.create({
    gambar: {
        width: 170,
        height: 120,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        marginBottom: 10,
    },
    position: {
        fontSize: 14,
        fontFamily: fonts.primary.medium,
        textAlign: 'center'
    },
    card: {
        backgroundColor: colors.grey,
        width: 170,
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingBottom: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    tombol: {
        color: 'white',
        backgroundColor: colors.primary,
        padding: 10,
        textAlign: 'center',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginBottom: 20,
        fontFamily: fonts.primary.bold
    }
})