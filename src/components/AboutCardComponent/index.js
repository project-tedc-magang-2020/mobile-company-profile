import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import {Client, SuccessProject} from '../../assets';
import { fonts } from '../../utils';

export default class AboutCardComponent extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.card}>
          <View style={styles.card1}>
            <View style={{marginRight: 10}}>
              <SuccessProject />
            </View>
            <View>
              <Text style={styles.text}>12</Text>
              <Text style={styles.text1}>Success Project</Text>
            </View>
          </View>
          <View style={styles.card1}>
            <View style={{marginRight: 10}}>
              <Client />
            </View>
            <View>
              <Text style={styles.text}>16</Text>
              <Text style={styles.text1}>Client</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginHorizontal: 30,
    marginBottom: 20,
    borderRadius: 10,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  card: {
    flexDirection: 'row',
  },
  card1: {
    flexDirection: 'row',
    margin: 10,
  },
  text: {
    fontSize: 25,
    fontFamily: fonts.primary.bold,
    color: '#FFBC0F',
  },
  text1: {
    fontSize: 12,
    fontFamily: fonts.primary.regular,
    color: 'black'
  }
});
