import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  Home,
  HomeActive,
  About,
  AboutActive,
  Contact,
  ContactActive
} from '../../assets';
import React from 'react';
import { colors, fonts } from '../../utils'

const TabItem = ({isFocused, onPress, onLongPress, label}) => {
  const Icon = () => {
    if (label === 'Home') {
      return isFocused ? <HomeActive /> : <Home />;
    }
    if (label === 'About') {
      return isFocused ? <AboutActive /> : <About />;
    }
    if (label === 'Contact') {
      return isFocused ? <ContactActive /> : <Contact />;
    }

    return <Home />;
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <Icon style={styles.icon}/>
      <Text style={styles.text(isFocused)}>{label}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    icon: {
        width: 30,
    },
    text: (isFocused) => ({
        color: isFocused ? colors.primary : '#000000',
        fontSize: 11,
        marginTop: 5,
        fontFamily: fonts.primary.regular
    })
});
