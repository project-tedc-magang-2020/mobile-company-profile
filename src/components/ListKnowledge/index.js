import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {CardKnowledge} from '../../components';

const ListKnowledge = ({knowledges}) => {
  return (
    <View>
      {knowledges.map(knowledge => {
        return <CardKnowledge key={knowledge.id} knowledge={knowledge} />;
      })}
    </View>
  );
};

export default ListKnowledge;

const styles = StyleSheet.create({});
