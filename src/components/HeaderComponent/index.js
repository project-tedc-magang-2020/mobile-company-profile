import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { colors, fonts } from '../../utils'

export default class HeaderComponent extends Component {
  render() {
    return (
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={[colors.primary, colors.secondary]}
        style={styles.container}>
        <View style={styles.headerTextSection}>
          <Text style={styles.headerText}>Company Profile</Text>
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 200,
  },
  headerTextSection: {
    marginTop: 15,
    marginHorizontal: 30,
  },
  headerText: {
    fontSize: 20,
    color: '#FFFFFF',
    fontFamily: fonts.primary.bold,
  },
});
