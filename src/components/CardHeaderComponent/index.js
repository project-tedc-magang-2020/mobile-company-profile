import {Text, StyleSheet, View, Image} from 'react-native';
import React, {Component} from 'react';
import { colors, fonts } from '../../utils';

export default class CardHeaderComponent extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.row1}>
          <View style={styles.logo}>
            <Image source={require('../../assets/images/logo-company.png')} />
          </View>
          <Text style={styles.crop}>
            <Text style={{color: colors.primary}}>Crop</Text> Inspirasi Digital
          </Text>
        </View>
        <View style={styles.lineStyle} />
        <Text style={styles.text}>
          <Text>
            Menjadi solusi berbagai hal dibidang IT secara kreatif, inovatif dan
            professional berlandasan kebersamaan untuk memberikan solusi bisnis
            anda.
          </Text>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 30,
    marginTop: -120,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  logo: {
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  row1: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 20,
  },
  crop: {
    fontSize: 20,
    fontFamily: fonts.primary.bold,
    color: 'black',
    marginHorizontal: 10,
  },
  lineStyle: {
    borderWidth: 1,
    borderColor: '#F3F0F0',
  },
  text: {
    fontSize: 12,
    fontFamily: fonts.primary.regular,
    color: '#585454',
    marginHorizontal: 20,
    marginVertical: 10,
  },
});
