import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { colors, fonts } from '../../utils'

const CardKnowledge = ({knowledge}) => {
  return (
    <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
      <TouchableOpacity style={styles.card}>
        <Image source={knowledge.image} style={styles.image} />
        <Text style={styles.tanggal}>{knowledge.tanggal}</Text>
        <Text style={styles.judul}>{knowledge.judul}</Text>
        <Text style={styles.konten}>{knowledge.konten}</Text>
      </TouchableOpacity>
    </View>
  )
}

export default CardKnowledge

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#FFF',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  image: {
    width: '100%',
    height: 220,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  tanggal: {
    marginHorizontal: 10,
    fontSize: 14,
    fontFamily: fonts.primary.medium,
    marginTop: 10,
    color: colors.secondary
  },
  judul: {
    fontSize: 18,
    fontFamily: fonts.primary.semiBold,
    color: colors.black,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  konten: {
    fontSize: 13,
    fontFamily: fonts.primary.regular,
    color: 'black',
    marginHorizontal: 10,
    marginBottom: 10,
    lineHeight: 20,
  },
})