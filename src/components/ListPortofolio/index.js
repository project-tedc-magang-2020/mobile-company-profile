import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import { CardPortofolio } from '../../components';

const ListPortofolio = ({portofolios}) => {
  return (
    <View style={styles.container}>
      {portofolios.map(portofolio => {
        return <CardPortofolio key={portofolio.id} portofolio={portofolio} />;
      })}
    </View>
  );
};

export default ListPortofolio;

const styles = StyleSheet.create({
    container: {
        marginTop: -90,
        marginHorizontal: 20,
    }
});
