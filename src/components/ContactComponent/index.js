import { Text, StyleSheet, View } from 'react-native'
import React, { Component } from 'react'
import { Address, Email } from '../../assets'
import { fonts } from '../../utils'

export default class ContactComponent extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.address}>
            <Address/>
            <View style={{ marginLeft:20 }}>
                <Text style={styles.addressTitle}>Address</Text>
                <Text style={styles.addressContent}>Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, Kota Bandung, Jawa Barat 40152</Text>
            </View>
        </View>
        <View style={styles.email}>
            <Email />
            <View style={{ marginLeft:15 }}>
                <Text style={styles.emailTitle}>Email</Text>
                <Text style={styles.emailContent}>info@crop.co.id {'\n'}crop.indonesia@gmail.com</Text>
            </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container:{
        marginTop: 30,
        marginHorizontal: 30,
    },
    address: {
        flexDirection: 'row',
        marginBottom: 30,
    },
    addressTitle: {
        fontSize: 20,
        fontFamily: fonts.primary.bold,
        color: 'black'
    },
    addressContent: {
        marginTop: 5,
        fontSize: 14,
        fontFamily: fonts.primary.regular,
        color: 'black',
        lineHeight: 20,
    },
    email: {
        flexDirection: 'row',
        marginBottom: 10,
    },
    emailTitle: {
        fontSize: 20,
        fontFamily: fonts.primary.bold,
        color: 'black'
    },
    emailContent: {
        marginTop: 5,
        fontSize: 14,
        fontFamily: fonts.primary.regular,
        color: 'black',
        lineHeight: 20,
    },
})