import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import { colors, fonts } from '../../utils';

const CardPortofolio = ({portofolio}) => {
  return (
    <View>
      <TouchableOpacity style={styles.card}>
        <Image source={portofolio.image} style={styles.image} />
        <Text style={styles.project}>{portofolio.project}</Text>
        <Text style={styles.client}>{portofolio.client}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CardPortofolio;

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#FFF',
    borderRadius: 10,
    marginBottom: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  image: {
    width: '100%',
    height: 220,
    borderRadius: 10,
  },
  project: {
    fontSize: 16,
    fontFamily: fonts.primary.semiBold,
    color: colors.secondary,
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 5,
  },
  client: {
    fontSize: 13,
    fontFamily: fonts.primary.regular,
    color: 'black',
    marginHorizontal: 10,
    marginBottom: 10,
  },
});
