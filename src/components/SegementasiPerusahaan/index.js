import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import {
  SegmentasiPerusahaan1,
  SegmentasiPerusahaan2,
  SegmentasiPerusahaan3,
} from '../../assets';
import {SliderBox} from 'react-native-image-slider-box';
import { colors } from '../../utils';

export default class SegmentasiPerusahaan extends Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [
        SegmentasiPerusahaan1,
        SegmentasiPerusahaan2,
        SegmentasiPerusahaan3,
      ],
    };
  }

  render() {
    return (
      <View style={{ marginBottom: 20 }}>
        <SliderBox
          images={this.state.images}
          autoplay
          circleLoop
          sliderBoxHeight={250}
          ImageComponentStyle={styles.slider}
          dotStyle={styles.dotStyle}
          dotColor={colors.primary}
          imageLoadingColor={colors.primary}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    slider: {
        borderRadius: 10,
        width: 340
    },
    dotStyle: {
        width: 50,
        height: 3,
        borderRadius: 5,
    }
});
