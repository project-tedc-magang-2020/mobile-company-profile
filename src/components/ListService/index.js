import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {CardService} from '../../components';

const ListService = ({services}) => {
  return (
    <View style={styles.container}>
      {services.map(service => {
        return <CardService key={service.id} service={service} />;
      })}
    </View>
  );
};

export default ListService;

const styles = StyleSheet.create({
    container: {
        marginTop: -60,
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    }
});
