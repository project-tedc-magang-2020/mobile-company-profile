import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { CardCareer } from '../../components'

const ListCareer = ({careers}) => {
  return (
    <View style={styles.container}>
      {careers.map((career) => {
          return (
              <CardCareer key={career.id} career={career} />
          )
      })}
    </View>
  )
}

export default ListCareer

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    }
})